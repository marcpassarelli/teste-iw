
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation'
import { SaveInfo } from './src/screens/SaveInfo'
import { ReadInfo } from './src/screens/ReadInfo'

const AppStack = createStackNavigator({
  SaveInfo: { screen: SaveInfo },
  ReadInfo: { screen: ReadInfo }
})

export const App = createAppContainer(AppStack);

export default App;
