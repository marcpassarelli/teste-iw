import database from '@react-native-firebase/database';

export async function salvarInformacao(nome, telefone, sexo, onSalvar){
  const ref = database().ref('/usuarios/');
  await ref.push({
    nome: nome,
    telefone: telefone,
    sexo: sexo
  });
  onSalvar()
}

export async function lerInformacao(){
  const ref = database().ref('/usuarios/');
  return ref.once('value').then(snapshot => snapshot)
}
