import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  SafeAreaView
} from 'react-native';
import {lerInformacao} from '../firebase'

var info = []

export class ReadInfo extends Component {

  static navigationOptions = ({navigation}) => ({
    title: "Usuários Salvos"
  });

  constructor(props){
    super(props);
    this.state = {
      loading:false,
      info:[]
    }
  }

  componentDidMount(){
    //zerar o array info
    info = []

    //ativa o ActivityIndicator até a informação ser carregada
    this.setState({
      loading: true
    });

    //chama função que le as informações do Firebase e retorna uma promise
    lerInformacao().then((snap)=>{
      var id=0
      snap.forEach((child)=>{
        //salva as informações recebidas do firebase em info
        info.push({
          nome: child.val().nome,
          telefone: child.val().telefone,
          sexo: child.val().sexo,
          id: id++
        })
      })
    }).then(()=>{
      //seta loading para false para sumir o ActivityIndicator e aparecer a Flatlist com as informações
      this.setState({
        loading: false
      });
    })
  }




  render() {

    const content= this.state.loading?

    <View style={{ flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'}}>
      <ActivityIndicator size="large" color={"black"} />
    </View> :

    <View>
      <FlatList
        data={info}
        renderItem={({ item }) => <Item nome={item.nome} telefone={item.telefone} sexo={item.sexo} />}
        keyExtractor={item => item.id.toString()}
      />
    </View>

    return (
      <SafeAreaView style={styles.container}>
        {content}
      </SafeAreaView>
    );
  }
}

//repsonsavel por renderizar cada célula da Flatlist de acordo com o array info
function Item({ nome, telefone, sexo }) {
  return (
    <View style={styles.item}>
      <View></View>
      <Text style={styles.textoNome}>Nome: {nome}</Text>
      <Text style={styles.textoNome}>Telefone: {telefone}</Text>
      <Text style={styles.textoNome}>Sexo: {sexo}</Text>
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: '#d1d1d1',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  textoNome: {
    fontSize: 18,
  },
});
