import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Picker,
  ActionSheetIOS,
  Platform,
  SafeAreaView,
  TouchableOpacity
} from 'react-native';
import {salvarInformacao} from '../firebase'
import { StackActions } from 'react-navigation';

var SEXOS = [
  'Feminino',
  'Masculino',
  'Cancelar',
];

export class SaveInfo extends Component {

  static navigationOptions = {
     header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      textoNome: '',
      textoFone:'',
      textoSexo:'Feminino'

    };
  }

  //Função para mostrar o ActionSheetIOS
  onSelectSexo() {
    ActionSheetIOS.showActionSheetWithOptions(
    { options: SEXOS,
      cancelButtonIndex: 2},
    (buttonIndex) => {
      if(buttonIndex==2){
        this.setState({ textoSexo: this.state.textoSexo })
      }else{
        this.setState({ textoSexo: SEXOS[buttonIndex] });
      }
    })
  }

  //Navegar para a página de ler informações
  goToReadInfo(){
    const pushAction = StackActions.push({
      routeName: 'ReadInfo'
    });
    this.props.navigation.dispatch(pushAction);
  }

  //
  salvarInformacoes(){
    //Verifica se os campos estão preenchidos
    if(this.state.textoNome && this.state.textoFone && this.state.textoSexo){
      //Caso os campos estejam preenchidos mas o número do telefone não tenha 11 caracteres
      if(this.state.textoFone.length!=11){
        alert('Número de telefone inválido')
      }else{
        //chama função para salvar as informações do formulário no Firebase
        salvarInformacao(this.state.textoNome,this.state.textoFone,this.state.textoSexo,
          ()=>{
            //callback para zerar os campos após o salvamento na db
            this.setState({
              textoNome:'',
              textoFone:''
            },function(){
              alert("Informações salvas com sucesso!")
            })
          }
        )
      }
    }else{
      alert('Preencha todos os campos')
    }
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Text style={{fontSize: 22,alignSelf: 'center'}}>Cadastrar Informações</Text>
        <Text style={styles.texto}>Nome: </Text>
        <TextInput
          style={styles.containerTextInput}
          onChangeText={(text) => this.setState({textoNome: text})}
          value={this.state.textoNome}
          autoCapitalize="words"
        />
        <Text style={styles.texto}>Telefone: </Text>
        <TextInput
          style={styles.containerTextInput}
          onChangeText={(text) => this.setState({textoFone:text})}
          value={this.state.textoFone}
          keyboardType="numeric"
          maxLength={11}
          placeholder="DDD+NUMERO, SEM ESPAÇOS"
        />

        <Text style={styles.texto}>Sexo: </Text>

        {/*Verifica o SO do usuário. Caso for iOS mostrará uma opção com ActionSheetIOS e se for Android, o Picker*/}

        {
          Platform.OS === 'ios'?

          <TouchableOpacity style={styles.containerActionSheet} onPress={()=>{this.onSelectSexo()}}>
            <Text style={styles.textActionSheet}>
              {this.state.textoSexo}
            </Text>
          </TouchableOpacity>

          :

          <Picker
            selectedValue={this.state.textoSexo}
            style={{height: 50, width: 150}}
            onValueChange={(item, itemIndex) =>
              this.setState({textoSexo: item})
            }>
            <Picker.Item label="Feminino" value="Feminino" color="blue" />
            <Picker.Item label="Masculino" value="Masculino" color="blue"/>
          </Picker>
        }

        <TouchableOpacity
          style={styles.containerButton}
          onPress={()=>{this.salvarInformacoes()}}>
          <Text style={styles.textButton}>SALVAR INFORMAÇÕES</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.containerButton} onPress={()=>{this.goToReadInfo()}}>
          <Text style={styles.textButton}>VER USUÁRIOS SALVOS</Text>
        </TouchableOpacity>

      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerTextInput:{
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginHorizontal: 5,
    marginBottom: 10,
    color: 'blue',
    paddingLeft: 5,
  },
  texto:{
    fontSize: 18,
    marginLeft: 5
  },
  containerButton:{
    height: 45,
    width: 200,
    marginHorizontal: 5,
    marginVertical: 5,
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: '#d1d1d1'
  },
  textButton:{
    fontSize: 16,
    alignSelf: 'center'
  },
  containerActionSheet:{
    justifyContent: 'center',
    marginBottom:10,
    borderWidth:1,
    height: 40,
    marginHorizontal: 5
  },
  textActionSheet:{
    marginLeft: 5,
    fontSize: 16,
    color:'blue'
  }
});
